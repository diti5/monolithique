package sn.kader.monolithique.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PrestationRequest {
    private String title;
    private int nombre;
}
