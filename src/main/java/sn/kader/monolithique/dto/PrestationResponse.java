package sn.kader.monolithique.dto;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrestationResponse {
    private String titre;
    private int nombre;
}
