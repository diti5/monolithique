package sn.kader.monolithique.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.kader.monolithique.dto.PrestationRequest;
import sn.kader.monolithique.dto.PrestationResponse;
import sn.kader.monolithique.service.PrestationService;

@RestController
public class PrestationController {
    private final PrestationService prestationService;

    public PrestationController( PrestationService prestationService) {
        this.prestationService = prestationService;
    }

    @GetMapping("/prestations")
    ResponseEntity<PrestationResponse> getPrestations(){
        return prestationService.getPrestations();
    }

    @GetMapping("/prestation/{id}")
    ResponseEntity<PrestationResponse> getPrestation(@PathVariable("id") int id){
        return prestationService.getPrestation(id);
    }

    @PostMapping("/prestations")
    ResponseEntity<PrestationResponse> savePrestation(@RequestBody PrestationRequest request){
        return prestationService.addPrestation(request);
    }

}
