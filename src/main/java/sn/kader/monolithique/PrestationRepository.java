package sn.kader.monolithique;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.kader.monolithique.entities.Prestation;

@Repository
public interface PrestationRepository extends JpaRepository<Prestation, Long> {
}
