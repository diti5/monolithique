package sn.kader.monolithique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonolithiqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonolithiqueApplication.class, args);
	}

}
