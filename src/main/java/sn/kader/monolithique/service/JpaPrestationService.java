package sn.kader.monolithique.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sn.kader.monolithique.PrestationRepository;
import sn.kader.monolithique.dto.PrestationRequest;
import sn.kader.monolithique.dto.PrestationResponse;
import sn.kader.monolithique.entities.Prestation;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("jpa")
@Profile("prod")
public class JpaPrestationService implements PrestationService {
    private final PrestationRepository prestationRepository;

    public JpaPrestationService(PrestationRepository prestationRepository) {
        this.prestationRepository = prestationRepository;
    }

    @Override
    public ResponseEntity<PrestationResponse> getPrestations() {
        long count = prestationRepository.count();
        return ResponseEntity.ok(PrestationResponse
                .builder()
                .nombre((int) count)
                .titre("Liste des prestations")
                .build()
        );
    }

    @Override
    public ResponseEntity<PrestationResponse> getPrestation(int id) {
        Optional<Prestation> optional = prestationRepository.findById((long)id);
        int nombre = optional.map( p -> 1).orElse(0);
        return ResponseEntity.ok(
                PrestationResponse
                .builder()
                .titre("La prestation demandée")
                .nombre(nombre)
                .build()
        );
    }

    @Override
    public ResponseEntity<PrestationResponse> addPrestation(PrestationRequest request) {
        Prestation prestation = new Prestation();
        prestation.setTitle(request.getTitle());
        prestation.setNombre(request.getNombre());
        prestationRepository.save(prestation);
        return getPrestations();
    }
}
