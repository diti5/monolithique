package sn.kader.monolithique.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sn.kader.monolithique.dto.PrestationRequest;
import sn.kader.monolithique.dto.PrestationResponse;

@Slf4j
@Service
@Qualifier("simple")
@Profile("dev")
public class SimplePrestationService implements PrestationService{
    @Override
    public ResponseEntity<PrestationResponse> getPrestations() {
        PrestationResponse prestationResponse = new PrestationResponse();
        prestationResponse.setTitre("Listes des prestations");
        prestationResponse.setNombre(5);
        log.info("Response {}", prestationResponse);
        return ResponseEntity.ok(prestationResponse);
    }

    @Override
    public ResponseEntity<PrestationResponse> getPrestation(int id) {
        return null;
    }

    @Override
    public ResponseEntity<PrestationResponse> addPrestation(PrestationRequest request) {
        return null;
    }
}
