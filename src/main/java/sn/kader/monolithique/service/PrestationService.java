package sn.kader.monolithique.service;

import org.springframework.http.ResponseEntity;
import sn.kader.monolithique.dto.PrestationRequest;
import sn.kader.monolithique.dto.PrestationResponse;

public interface PrestationService {
    ResponseEntity<PrestationResponse> getPrestations();

    ResponseEntity<PrestationResponse> getPrestation(int id);

    ResponseEntity<PrestationResponse> addPrestation(PrestationRequest request);
}
